import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WishListService } from './shared/wish-list.service';

import emailjs from 'emailjs-com';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  emailForm: FormGroup;
  selectedCategory: any;
  wishList: any;
  categories = [
    { label: 'Chasuables', value: 'chasuables' },
    { label: 'Copes', value: 'copes' },
    { label: 'Stoles', value: 'stoles' },
    { label: 'Dalmatics', value: 'dalmatics' },
    { label: 'High Mass Sets', value: 'highMassSets' },
    { label: 'Mitres', value: 'mitres' },
    { label: 'Altar Frontols', value: 'altarFrontals' },
    { label: 'Canopy', value: 'canopy' },
  ];
  public showWishListed: boolean;
  constructor(
    private fb: FormBuilder,
    private wishListService: WishListService
  ) {}
  ngOnInit() {
    this.showWishListed = false;
    this.selectedCategory = this.categories[0];

    this.wishListService.wishList.subscribe((value) => {
      this.wishList = value;
      console.log(this.wishList);
    });

    window.addEventListener('scroll', () => {
      const navbar = document.getElementById('navbar');
      if (navbar) {
        if (window.scrollY > 140) {
          navbar.classList.add('navbar-sticky');
        } else {
          navbar.classList.remove('navbar-sticky');
        }
      }
    });
    this.formInit();
  }

  formInit() {
    this.emailForm = this.fb.group({
      name: ['', Validators.required],
      userEmail: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', [Validators.required, Validators.minLength(10)]],
    });
  }

  checkFormValidity(formControlName: string) {
    const frmCntrl = this.emailForm.controls[formControlName];
    if ((frmCntrl.touched || frmCntrl.dirty) && frmCntrl.errors) {
      return true;
    }
    return false;
  }

  changeCategory(selectedCategory) {
    this.selectedCategory = selectedCategory;
    this.openCategoriesList();
  }

  openCategoriesList() {
    const navDrpBtn = document.getElementById('navDrpBtn');
    if (!navDrpBtn) {
      return;
    }
    navDrpBtn.classList.toggle('navbar-btn--clicked');
    const navbarMenu = document.getElementById('navBarMenu');
    navbarMenu.classList.toggle('clicked');
  }

  showWishlistPage() {
    this.showWishListed = !this.showWishListed;
  }

  sendMail() {
    const serviceId = 'service_h9kgp0z';
    const templateID = 'template_a8ve8z9';
    const userID = 'user_BG1vHNEJB8y32UDY8faKp';
    const formValue = this.emailForm.value;
    emailjs
      .send(
        serviceId,
        templateID,
        {
          from_name: formValue.name,
          to_name: 'CHASABLES',
          message: this.formMessage()
        },
        userID
      )
      .then(
        (response) => {
          console.log('SUCCESS!', response.status, response.text);
        },
        (err) => {
          console.log('FAILED...', err);
        }
      );
  }

  focusClose() {
    (document.getElementById('closeModal') as HTMLButtonElement).focus();
  }

  formMessage() {
    const formValue = this.emailForm.value;
    let message = "EMAIL: " + formValue.userEmail + "<br>";
    message = message + " Phone: " + formValue.phoneNumber + "\n";

    let wishList = "";
    this.wishList.map((item, index) => {
      wishList =
        wishList +
        "\n" +
        (index + 1) +
        ". " +
        item.details.id +
        " " +
        item.details.title;
    });
    return message + wishList;
  }
}
