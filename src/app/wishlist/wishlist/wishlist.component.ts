import { Component, OnInit } from '@angular/core';
import { WishListService } from 'src/app/shared/wish-list.service';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.scss']
})
export class WishlistComponent implements OnInit {

  wistListItems;

  constructor(
    private wishListService: WishListService
  ) { }

  ngOnInit(): void {
    this.wishListService.wishList.subscribe((val) => {
      this.wistListItems = val;
    });
  }


  removeItem(product) {
    this.wishListService.updateWishlist(product);
    const index = this.wistListItems.findIndex(item => item.name === product.name);
    if (index !== -1) {
      product.wishList = true;
    } else {
      product.wishList = false;
    }
  }
}
