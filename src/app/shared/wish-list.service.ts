import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WishListService {

  public wishList = new BehaviorSubject<any>([]);
  constructor() { }

  updateWishlist(product) {
    const wishListItems: Array<any> = this.wishList.value;
    const isElementPresent = wishListItems.findIndex(ele => ele.name === product.name);
    if (isElementPresent === -1) {
      wishListItems.push(product);
    } else {
      wishListItems.splice(isElementPresent, 1);
    }
    this.wishList.next(wishListItems);
  }
}
