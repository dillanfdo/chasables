import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products/products.component';
import { WishlistComponent } from './wishlist/wishlist/wishlist.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    WishlistComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
