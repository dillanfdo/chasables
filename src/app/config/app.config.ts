export const IMGAES = {
  chasuables: [
    {
      src: [
        'assets/images/DressMaterials/image10.jpg',
        'assets/images/DressMaterials/image1.jpg',
        'assets/images/DressMaterials/image4.jpg',
      ],
      name: 'Card10',
      details: {
        title: 'Title',
        category: 'Chasuables',
        id: 'CH01',
        price: 'Rs. 2000',
        size: {},
      },
    },
    {
      src: [
        'assets/images/DressMaterials/image9.jpg',
        'assets/images/DressMaterials/image1.jpg',
        'assets/images/DressMaterials/image4.jpg',
      ],
      name: 'Card9',
      details: {
        title: 'Title',
        category: 'Chasuables',
        id: 'CH02',
        price: 'Rs. 2000',
        size: {},
      },
    },
    {
      src: ['assets/images/DressMaterials/image8.jpg'],
      name: 'Card8',
      details: {
        title: 'Title',
        category: 'Chasuables',
        id: 'CH03',
        price: 'Rs. 2000',
        size: {},
      },
    },
    {
      src: ['assets/images/DressMaterials/image1.jpg'],
      name: 'Card1',
      details: {
        title: 'Title',
        category: 'Chasuables',
        id: 'CH04',
        price: 'Rs. 2000',
        size: {},
      },
    },
    {
      src: ['assets/images/DressMaterials/image2.jpg'],
      name: 'Card2',
      details: {
        title: 'Title',
        category: 'Chasuables',
        id: 'CH05',
        price: 'Rs. 2000',
        size: {},
      },
    },
    {
      src: ['assets/images/DressMaterials/image3.jpg'],
      name: 'Card3',
      details: {
        title: 'Title',
        category: 'Chasuables',
        id: 'CH06',
        price: 'Rs. 2000',
        size: {},
      },
    },
    {
      src: ['assets/images/DressMaterials/image4.jpg'],
      name: 'Card4',
      details: {
        title: 'Title',
        category: 'Chasuables',
        id: 'CH07',
        price: 'Rs. 2000',
        size: {},
      },
    },
    {
      src: ['assets/images/DressMaterials/image5.jpg'],
      name: 'Card5',
      details: {
        title: 'Title',
        category: 'Chasuables',
        id: 'CH08',
        price: 'Rs. 2000',
        size: {},
      },
    },
  ],
  copes: [
    {
      src: ['assets/images/DressMaterials/image1.jpg'],
      name: 'Card1',
      details: {
        title: 'Title',
        category: 'Chasuables',
        id: 'CO01',
        price: 'Rs. 2000',
        size: {},
      },
    },
    {
      src: ['assets/images/DressMaterials/image2.jpg'],
      name: 'Card2',
      details: {
        title: 'Title',
        category: 'Chasuables',
        id: 'CO02',
        price: 'Rs. 2000',
        size: {},
      },
    },
    {
      src: ['assets/images/DressMaterials/image3.jpg'],
      name: 'Card3',
      details: {
        title: 'Title',
        category: 'Chasuables',
        id: 'CO03',
        price: 'Rs. 2000',
        size: {},
      },
    },
    {
      src: ['assets/images/DressMaterials/image4.jpg'],
      name: 'Card4',
      details: {
        title: 'Title',
        category: 'Chasuables',
        id: 'CO04',
        price: 'Rs. 2000',
        size: {},
      },
    },
    {
      src: ['assets/images/DressMaterials/image7.jpg'],
      name: 'Card7',
      details: {
        title: 'Title',
        category: 'Chasuables',
        id: 'CO05',
        price: 'Rs. 2000',
        size: {},
      },
    },
    {
      src: ['assets/images/DressMaterials/image8.jpg'],
      name: 'Card8',
      details: {
        title: 'Title',
        category: 'Chasuables',
        id: 'CO06',
        price: 'Rs. 2000',
        size: {},
      },
    },
    {
      src: ['assets/images/DressMaterials/image9.jpg'],
      name: 'Card9',
      details: {
        title: 'Title',
        category: 'Chasuables',
        id: 'CO07',
        price: 'Rs. 2000',
        size: {},
      },
    },
    {
      src: ['assets/images/DressMaterials/image10.jpg'],
      name: 'Card10',
      details: {
        title: 'Title',
        category: 'Chasuables',
        id: 'CO08',
        price: 'Rs. 2000',
        size: {},
      },
    },
  ],
  stoles: [
    {
      src: ['assets/images/DressMaterials/image6.jpg'],
      name: 'Card6',
      details: {
        title: 'Title',
        category: 'Chasuables',
        price: 'Rs. 2000',
        size: {},
      },
    },
    {
      src: ['assets/images/DressMaterials/image7.jpg'],
      name: 'Card7',
      details: {
        title: 'Title',
        category: 'Chasuables',
        price: 'Rs. 2000',
        size: {},
      },
    },
    {
      src: ['assets/images/DressMaterials/image5.jpg'],
      name: 'Card5',
      details: {
        title: 'Title',
        category: 'Chasuables',
        price: 'Rs. 2000',
        size: {},
      },
    },
    {
      src: ['assets/images/DressMaterials/image6.jpg'],
      name: 'Card6',
      details: {
        title: 'Title',
        category: 'Chasuables',
        price: 'Rs. 2000',
        size: {},
      },
    },
  ],
  dalmatics: [],
  highMassSets: [],
  mitres: [],
  altarFrontals: [],
  canopy: [],
};
