import { Component, Input, OnInit } from '@angular/core';
import { IMGAES } from 'src/app/config/app.config';
import { WishListService } from 'src/app/shared/wish-list.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  _selectedCategory: any;
  selectedProduct: any;
  products: Array<any>;
  openModalBtn: HTMLElement;
  wishListItems;

  @Input()  set selectedCategory(value: any) {
    this._selectedCategory = value;
    this.products = IMGAES[this._selectedCategory.value];
  }
  constructor(
    private wishListService: WishListService
  ) { }

  ngOnInit(): void {
    this.openModalBtn = document.getElementById('open-modal');
    this.selectedProduct = this.products[0];
    this.wishListService.wishList.subscribe(val => {
      this.wishListItems = val;
    })
  }

  openProduct(product) {
    this.selectedProduct = product;
    this.openModalBtn.click();
  }


  addOrRemoveItem(product) {
    this.wishListService.updateWishlist(product);
    const index = this.wishListItems.findIndex(item => item.name === product.name);
    if (index !== -1) {
      product.wishList = true;
    } else {
      product.wishList = false;
    }
  }
}
